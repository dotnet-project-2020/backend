﻿using System.ComponentModel.DataAnnotations;

namespace Core.Specifications
{
    public class ProductSpecParams
    {
        private const int MaxPerPage = 50;

        private int _perPage = 15;

        [Range(1, int.MaxValue)] public int Page { get; set; } = 1;

        [Range(1, int.MaxValue)]
        public int PerPage
        {
            get => _perPage;
            set => _perPage = value > MaxPerPage ? MaxPerPage : value;
        }

        public int? BrandId { get; set; }

        public int? TypeId { get; set; }

        public string Sort { get; set; }

        private string _search;

        public string Search
        {
            get => _search;
            set => _search = value.ToLower();
        }
    }
}