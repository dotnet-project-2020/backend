using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Entities
{
    public class ProductAttributeValue : BaseEntity
    {
        
        public int ProductId { get; set; }
        
        public int AttributeValueId { get; set; }

        [Required] public Product Product { get; set; }
        
        [Required] public AttributeValue AttributeValue { get; set; }

        [Column(TypeName = "decimal(16)")]
        [Required]
        public decimal Price { get; set; }
        
        public int Quantity { get; set; }
        
        public DateTimeOffset Created { get; set; } = DateTimeOffset.Now;

        public DateTimeOffset Updated
        {
            get => _dateCreated.HasValue
                ? _dateCreated.Value
                : DateTimeOffset.Now;
            set => _dateCreated = value;
        }

        private DateTimeOffset? _dateCreated;
    }
}