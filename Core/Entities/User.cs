using System.ComponentModel.DataAnnotations;

namespace Core.Entities
{
    public class User : BaseEntity
    {
        [Required] [StringLength(20)] public string UserName { get; set; }

        [Required] public string Password { get; set; }

        [Required] public string Fullname { get; set; }

        [Required] public string Email { get; set; }

        [Required] public string Phone { get; set; }

        public string Address { get; set; }

        public int IsSuperAdmin { get; set; }
        
    }
}