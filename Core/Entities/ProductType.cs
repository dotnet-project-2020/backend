﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Core.Entities
{
    public class ProductType : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public int? ParentId { get; set; }
    }
}