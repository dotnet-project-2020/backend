using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Entities
{
    public class ProductModels : BaseEntity
    {
        public int ProductId { get; set; }
        [Required] public Product Product { get; set; }

        public int? AttributeValueId { get; set; }
        public AttributeValue AttributeValue { get; set; }

        public int? AttributeValueSecondId { get; set; }
        public AttributeValue AttributeValueSecond { get; set; }

        [Column(TypeName = "decimal(16)")]
        [Required]
        public decimal Price { get; set; }

        [Column(TypeName = "decimal(16)")]
        [Required]
        public decimal OriginPrice { get; set; }

        public int Quantity { get; set; }

        public string PictureUrlList { get; set; }

        public DateTimeOffset Created { get; set; } = DateTimeOffset.Now;

        public DateTimeOffset Updated
        {
            get => _dateCreated.HasValue
                ? _dateCreated.Value
                : DateTimeOffset.Now;
            set => _dateCreated = value;
        }

        private DateTimeOffset? _dateCreated;
    }
}