using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Entities
{
    public class Product : BaseEntity
    {
        [Required] [StringLength(256)] public string CodeSKU { get; set; }

        [Required] [StringLength(256)] public string Name { get; set; }
        public string Description { get; set; }

        [Column(TypeName = "decimal(16)")]
        [Required]
        public decimal Price { get; set; }

        [Column(TypeName = "decimal(16)")]
        [Required]
        public decimal OriginPrice { get; set; }

        public int? Quantity { get; set; } = 1;

        public int? TopDisplay { get; set; }

        public string PictureUrl { get; set; }

        public ProductType ProductType { get; set; }
        public int ProductTypeId { get; set; }

        public ProductBrand ProductBrand { get; set; }
        public int ProductBrandId { get; set; }

        public IReadOnlyList<ProductAttributeValue> ProductAttributeValues { get; set; }

        public bool ActiveFlag { get; set; } = false;

        public DateTimeOffset Created { get; set; } = DateTimeOffset.Now;

        public DateTimeOffset Updated
        {
            get => _dateCreated.HasValue
                ? _dateCreated.Value
                : DateTimeOffset.Now;
            set => _dateCreated = value;
        }

        private DateTimeOffset? _dateCreated;
    }
}