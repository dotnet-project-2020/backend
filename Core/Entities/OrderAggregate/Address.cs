﻿namespace Core.Entities.OrderAggregate
{
    public class Address
    {
        public Address()
        {
        }

        public Address(string fullname, string street, string ward, string city)
        {
            Fullname = fullname;
            Street = street;
            Ward = ward;
            City = city;
        }

        public string Fullname { get; set; }
        public string Street { get; set; }
        public string Ward { get; set; }
        public string City { get; set; }
    }
}