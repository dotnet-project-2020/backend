﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Core.Entities
{
    public class ProductBrand : BaseEntity
    {
        [Required] public string Name { get; set; }

        public string Image { get; set; }
    }
}