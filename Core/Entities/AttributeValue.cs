using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Core.Entities
{
    public class AttributeValue : BaseEntity
    {
        [Required] [StringLength(256)] public string Name { get; set; }

        public int AttributeId { get; set; }

        public Attribute Attribute { get; set; }

        public IReadOnlyList<ProductAttributeValue> ProductAttributeValues { get; set; }

        public DateTimeOffset Created { get; set; } = DateTimeOffset.Now;

        public DateTimeOffset Updated
        {
            get => _dateCreated.HasValue
                ? _dateCreated.Value
                : DateTimeOffset.Now;
            set => _dateCreated = value;
        }

        private DateTimeOffset? _dateCreated;
    }
}