﻿using System;
using System.Collections.Generic;

namespace API.Dtos
{
    public class ProductModelToReturnDto
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int? AttributeValueId { get; set; }
        public int? AttributeValueSecondId { get; set; }
        public decimal Price { get; set; }
        public decimal OriginPrice { get; set; }
        public int Quantity { get; set; }
        public string PictureUrlList { get; set; }
    }
}