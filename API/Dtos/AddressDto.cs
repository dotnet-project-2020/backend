﻿using System.ComponentModel.DataAnnotations;

namespace API.Dtos
{
    public class AddressDto
    {
        [Required] public string Fullname { get; set; }
        [Required] public string Street { get; set; }
        [Required] public string Ward { get; set; }
        [Required] public string City { get; set; }
    }
}