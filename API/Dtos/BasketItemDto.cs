﻿using System.ComponentModel.DataAnnotations;

namespace API.Dtos
{
    public class BasketItemDto
    {
        [Required] public int Id { get; set; }
        [Required] public string ProductName { get; set; }

        [Required]
        [Range(1000, double.MaxValue, ErrorMessage = "Price must be at least 1000")]
        public decimal Price { get; set; }

        [Required]
        [Range(1, double.MaxValue, ErrorMessage = "Quantity must be at least 1")]
        public int Quantity { get; set; }

        [Required] public string PictureUrl { get; set; }
        [Required] public string ProductBrand { get; set; }
        [Required] public string ProductType { get; set; }
    }
}