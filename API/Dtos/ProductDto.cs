using System.ComponentModel.DataAnnotations;

namespace API.Dtos
{
    public class ProductDto
    {
        [Required] public string CodeSKU { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string Description { get; set; }

        [Required]
        [Range(1000, double.MaxValue, ErrorMessage = "Price must be at least 1000")]
        public decimal Price { get; set; }

        [Range(1000, double.MaxValue, ErrorMessage = "Price must be at least 1000")]
        public decimal OriginPrice { get; set; }
        
        public decimal Quantity { get; set; }

        [Required] public string ProductBrandId { get; set; }
        [Required] public string ProductTypeId { get; set; }
    }
}