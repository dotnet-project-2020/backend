﻿using System;
using System.Collections.Generic;

namespace API.Dtos
{
    public class ProductModelVariationsToReturnDto
    {
        public string Name { get; set; }

        public IReadOnlyList<OptionPair> Options { get; set; }
    }
}