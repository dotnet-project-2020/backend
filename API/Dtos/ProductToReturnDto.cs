namespace API.Dtos
{
    public class ProductToReturnDto
    {
        public int Id { get; set; }
        public string CodeSKU { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public decimal OriginPrice { get; set; }

        public int Quantity { get; set; }
        public string PictureUrl { get; set; }
        public string ProductType { get; set; }
        public string ProductBrand { get; set; }

        public string Created { get; set; }
        public string Updated { get; set; }
    }
}