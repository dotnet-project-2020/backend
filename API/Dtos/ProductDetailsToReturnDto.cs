using System.Collections.Generic;
using Core.Entities;

namespace API.Dtos
{
    public class ProductDetailsToReturnDto : ProductToReturnDto
    {
        public List<ProductModelVariationsToReturnDto> Variations { get; set; }
        public List<ProductModelToReturnDto> Models { get; set; }
    }
}