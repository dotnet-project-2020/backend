﻿using System;
using System.Collections.Generic;

namespace API.Dtos
{
    public class OptionPair
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}