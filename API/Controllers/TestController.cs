using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Dtos;
using API.Errors;
using API.Helpers;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    public class TestController : BaseApiController
    {
        private readonly IGenericRepository<AttributeValue> _attributeValue;
        private readonly IGenericRepository<Attribute> _attributeValue2;
        private readonly StoreContext _context;

        public TestController(
            IGenericRepository<AttributeValue> attributeValue,
            IGenericRepository<Attribute> attributeValue2,
            StoreContext context
        )
        {
            _attributeValue = attributeValue;
            _attributeValue2 = attributeValue2;
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<Attribute>> GetProducts()
        {
            var products = await _attributeValue.ListAllAsync();

            return Ok(products);
        }

        [HttpGet("show/{id}")]
        public async Task<ActionResult> GetProductss(int id)
        {
            // var a =  await _context.Products.Include(e => e.ProductType)
            //     .ToListAsync();
            // var a = await _context.Attribute.Include(e => e.AttributeValues).ToListAsync();
            // var a = await _context.Products.ToListAsync();

            // var a = _context.ProductAttributeValue
            //     .Where(pav => pav.ProductId == 7);    


            // JOIN
            // var asad = await (
            //     from attr in _context.Attribute
            //     join a in _context.AttributeValue on attr.Id equals a.AttributeId
            //     join p in _context.ProductAttributeValue on a.Id equals p.AttributeValueId
            //     where p.ProductId == id
            //     select a
            // ).ToListAsync();

// select
//     DISTINCT  a3.Name, 
//     productdetail.id,  a.Id, a3.Name,a.Name , a2.Id,a5.Name,a2.Name, Price, OriginPrice,Quantity,  PictureUrlList, ProductId
// from productdetail
//          left join attributevalue a on a.Id = productdetail.AttributeValueId
//          left join attribute a3 on a3.Id = a.AttributeId
//          left join attributevalue a2 on a2.Id = productdetail.AttributeValueSecondId
//          left join attribute a5 on a5.Id = a2.AttributeId
// where ProductId = 9

            // var attribute = await _context.ProductDetail
            //     .Where(pav => pav.ProductId == id).ToListAsync();    

            var attributeFirst = (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select b.Name
            ).FirstOrDefault();
            var attributeFirstValues = await (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select a.Name
            ).Distinct().ToListAsync();
            var attributeSecond = (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueSecondId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select b.Name
            ).FirstOrDefault();
            var attributeSecondValues = await (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueSecondId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select a.Name
            ).Distinct().ToListAsync();

            // var firstModel = new ProductModelVariationsToReturnDto
            // {
            //     Name = attributeFirst,
            //     Options = attributeFirstValues
            // };
            // var secondModel = new ProductModelVariationsToReturnDto
            // {
            //     Name = attributeSecond,
            //     Options = attributeSecondValues
            // };
            
            
            var dsadsa = await (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueId equals a.Id
                join a2 in _context.AttributeValue on productDetail.AttributeValueSecondId equals a2.Id
                join a3 in _context.Attribute on a.AttributeId equals a3.Id
                join a5 in _context.Attribute on a2.AttributeId equals a5.Id
                where productDetail.ProductId == id
                select productDetail
                ).ToListAsync();
            // var theProduct = _context.Products.FindAsync
            return Ok(dsadsa);
        }
    }
}