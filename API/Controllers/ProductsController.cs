using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using API.Dtos;
using API.Errors;
using API.Helpers;
using AutoMapper;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using Infrastructure.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace API.Controllers
{
    public class ProductsController : BaseApiController
    {
        private readonly IGenericRepository<Product> _productRepo;
        private readonly IGenericRepository<ProductBrand> _productBrandRepo;
        private readonly IGenericRepository<ProductType> _productTypeRepo;
        private readonly IMapper _mapper;
        private StoreContext _context;
        private readonly ILogger _logger;
        private IWebHostEnvironment _environment;

        public ProductsController(IGenericRepository<Product> productRepo,
            IGenericRepository<ProductBrand> productBrandRepo,
            IGenericRepository<ProductType> productTypeRepo,
            IMapper mapper,
            StoreContext context,
            ILogger<ProductsController> logger,
            IWebHostEnvironment environment
        )
        {
            _productRepo = productRepo;
            _productBrandRepo = productBrandRepo;
            _productTypeRepo = productTypeRepo;
            _mapper = mapper;
            _context = context;
            _logger = logger;
            _environment = environment;
        }

        [HttpGet]
        public async Task<ActionResult<Pagination<ProductToReturnDto>>> GetProducts(
            [FromQuery] ProductSpecParams productParams)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(productParams);

            var countSpec = new ProductWithFilterForCountSpecification(productParams);

            var totalItems = await _productRepo.CountAsync(countSpec);

            var products = await _productRepo.ListAsync(spec);

            var data = _mapper.Map<IReadOnlyList<Product>, IReadOnlyList<ProductToReturnDto>>(products);

            return Ok(new Pagination<ProductToReturnDto>(productParams.Page, productParams.PerPage, totalItems,
                data));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProductDetailsToReturnDto>> GetProduct(int id)
        {
            var spec = new ProductsWithTypesAndBrandsSpecification(id);

            // variations 
            var attributeFirst = (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select b.Name
            ).FirstOrDefault();
            var attributeFirstValues = await (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select new OptionPair
                {
                    Id = a.Id,
                    Name = a.Name,
                }).Distinct().ToListAsync();
            var attributeSecond = (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueSecondId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select b.Name
            ).FirstOrDefault();
            var attributeSecondValues = await (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueSecondId equals a.Id
                join b in _context.Attribute on a.AttributeId equals b.Id
                where productDetail.ProductId == id
                select new OptionPair
                {
                    Id = a.Id,
                    Name = a.Name,
                }).Distinct().ToListAsync();

            List<ProductModelVariationsToReturnDto> list = new List<ProductModelVariationsToReturnDto>();

            if (attributeFirst != null)
            {
                var firstModel = new ProductModelVariationsToReturnDto
                {
                    Name = attributeFirst,
                    Options = attributeFirstValues
                };
                list.Add(firstModel);
            }

            if (attributeSecond != null)
            {
                var secondModel = new ProductModelVariationsToReturnDto
                {
                    Name = attributeSecond,
                    Options = attributeSecondValues
                };
                list.Add(secondModel);
            }

            var models = await (
                from productDetail in _context.ProductModels
                join a in _context.AttributeValue on productDetail.AttributeValueId equals a.Id
                join a2 in _context.AttributeValue on productDetail.AttributeValueSecondId equals a2.Id
                join a3 in _context.Attribute on a.AttributeId equals a3.Id
                join a5 in _context.Attribute on a2.AttributeId equals a5.Id
                where productDetail.ProductId == id
                select new ProductModelToReturnDto
                {
                    Id = productDetail.Id,
                    ProductId = productDetail.ProductId,
                    AttributeValueId = productDetail.AttributeValueId,
                    AttributeValueSecondId = productDetail.AttributeValueSecondId,
                    Price = productDetail.Price,
                    OriginPrice = productDetail.OriginPrice,
                    Quantity = productDetail.Quantity,
                    PictureUrlList = productDetail.PictureUrlList
                }
            ).ToListAsync();

            var product = await _productRepo.GetEntityWithSpec(spec);
            if (product == null) return NotFound(new ApiResponse(404));

            ProductToReturnDto productReturn2 = _mapper.Map<Product, ProductToReturnDto>(product);

            ProductDetailsToReturnDto productReturn =
                _mapper.Map<ProductToReturnDto, ProductDetailsToReturnDto>(productReturn2);

            productReturn.Variations = list;
            productReturn.Models = models;
            return Ok(productReturn);
        }

        [HttpPost]
        [Authorize]
        public async Task<ActionResult<ProductDetailsToReturnDto>> AddProduct([FromForm] ProductDto productDto,
            List<IFormFile> files)
        {
            // _logger.LogInformation("================================================================");

            var product = _mapper.Map<ProductDto, Product>(productDto);

            if (files == null || files.Count <= 0)
            {
                var errors = new Dictionary<string, string[]>();
                string[] error = {"Files field is required."};
                errors.Add("files", error);
                return new BadRequestObjectResult(new ApiValidationErrorResponse
                {
                    Errors = errors
                });
            }

            // store data
            await _context.Products.AddAsync(product);
            await _context.SaveChangesAsync();

            string thumbNail = null;
            thumbNail = Path.Combine("images/products/", product.Id.ToString(),
                files[0].FileName.FileToKebabCase()).Replace("\\", "/");

            product.PictureUrl = thumbNail;

            // update url
            _context.Products.Update(product);
            await _context.SaveChangesAsync();

            // store image
            foreach (var file in files)
            {
                string pathFolder = Path.Combine(_environment.WebRootPath,
                    "images/products/", product.Id.ToString());
                Directory.CreateDirectory(pathFolder);

                string pathImg = Path.Combine(pathFolder, file.FileName.FileToKebabCase());
                using (var stream = new FileStream(pathImg, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }

            return await GetProduct(product.Id);
        }

        [HttpGet("brands")]
        public async Task<ActionResult<ProductBrand>> GetProductBrands()
        {
            var list = await _productBrandRepo.ListAllAsync();
            return Ok(list);
        }

        [HttpGet("types")]
        public async Task<ActionResult<ProductType>> GetProductTypes()
        {
            var list = await _productTypeRepo.ListAllAsync();
            return Ok(list);
        }
    }
}