﻿using System.Collections.Generic;

namespace API.Helpers
{
    public class Pagination<T> where T : class
    {
        public Pagination(int page, int perPage, int total, IReadOnlyList<T> data)
        {
            int lastPage = (total / perPage) + 1;
            if (total % perPage == 0)
            {
                lastPage = total / perPage;
            }

            Meta = new PaginationMeta(page, perPage, lastPage, total);
            Data = data;
        }

        public PaginationMeta Meta { get; set; }

        public IReadOnlyList<T> Data { get; set; }
    }
}