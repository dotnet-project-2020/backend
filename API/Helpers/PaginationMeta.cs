﻿namespace API.Helpers
{
    public class PaginationMeta
    {
        public PaginationMeta(int page, int perPage, int lastPage, int total)
        {
            Page = page;
            PerPage = perPage;
            LastPage = lastPage;
            Total = total;
        }

        public int Page { get; set; }

        public int PerPage { get; set; }

        public int LastPage { get; set; }
        
        public int Total { get; set; }
    }
}