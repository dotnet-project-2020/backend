﻿using System.IO;
using System.Text.RegularExpressions;

namespace API.Helpers
{
    public static class Utils
    {
        public static string ToCamelCase(this string str)
        {
            return string.IsNullOrEmpty(str) || str.Length < 2
                ? str
                : char.ToLowerInvariant(str[0]) + str.Substring(1);
        }

        public static string ToKebabCase(this string value)
        {
            // Replace all non-alphanumeric characters with a dash
            value = Regex.Replace(value, @"[^0-9a-zA-Z]", "-");

            // Replace all subsequent dashes with a single dash
            value = Regex.Replace(value, @"[-]{2,}", "-");

            // Remove any trailing dashes
            value = Regex.Replace(value, @"-+$", string.Empty);

            // Remove any dashes in position zero
            if (value.StartsWith("-")) value = value.Substring(1);

            // Lowercase and return
            return value.ToLower();
        }

        public static string FileToKebabCase(this string value)
        {
            return Path.GetFileNameWithoutExtension(value).ToKebabCase() + Path.GetExtension(value);
        }

        // static void Main(string[] args)
        // {
        //     // Display the number of command line arguments.
        //     Console.WriteLine(Utils.ToKebabCase("Tai nghe Bluetooth Beats Flex MYMC2/ MYMD2"));
        // }
    }
}