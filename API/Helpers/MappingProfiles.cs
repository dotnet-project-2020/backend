﻿using API.Dtos;
using AutoMapper;
using Core.Entities;
using Core.Entities.OrderAggregate;
using Address = Core.Entities.Identity.Address;

namespace API.Helpers
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<Product, ProductToReturnDto>()
                .ForMember(d => d.ProductBrand,
                    o => o.MapFrom(s => s.ProductBrand.Name))
                .ForMember(d => d.ProductType,
                    o => o.MapFrom(s => s.ProductType.Name))
                .ForMember(d => d.PictureUrl,
                    o => o.MapFrom<ProductUrlResolver>())
                .ForMember(d => d.Created,
                    o => o.MapFrom(s => s.Created.ToString("O")))
                .ForMember(d => d.Updated,
                    o => o.MapFrom(s => s.Updated.ToString("O")));

            CreateMap<ProductToReturnDto, ProductDetailsToReturnDto>();

            CreateMap<ProductDto, Product>()
                .ForMember(d => d.ProductBrandId,
                    o => o.MapFrom(s => s.ProductBrandId))
                .ForMember(d => d.ProductTypeId,
                    o => o.MapFrom(s => s.ProductTypeId));

            CreateMap<Address, AddressDto>().ReverseMap();
            CreateMap<CustomerBasketDto, CustomerBasket>();
            CreateMap<BasketItemDto, BasketItem>();
            CreateMap<AddressDto, Core.Entities.OrderAggregate.Address>();
            CreateMap<Order, OrderToReturnDto>()
                .ForMember(d => d.DeliveryMethod,
                    o => o.MapFrom(s => s.DeliveryMethod.ShortName))
                .ForMember(d => d.ShippingPrice,
                    o => o.MapFrom(s => s.DeliveryMethod.Price));

            CreateMap<OrderItem, OrderItemDto>()
                .ForMember(d => d.ProductId,
                    o => o.MapFrom(s => s.ItemOrdered.ProductId))
                .ForMember(d => d.ProductName,
                    o => o.MapFrom(s => s.ItemOrdered.ProductName))
                .ForMember(d => d.PictureUrl,
                    o => o.MapFrom(s => s.ItemOrdered.PictureUrl))
                .ForMember(d => d.PictureUrl,
                    o => o.MapFrom<OrderItemUrlResolver>());
        }
    }
}