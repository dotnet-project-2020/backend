﻿using System;
using Core.Entities;
using Core.Entities.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Order = Core.Entities.OrderAggregate.Order;

namespace Infrastructure.Data.Config
{
    public class ProductAttributeValueConfiguration : IEntityTypeConfiguration<ProductAttributeValue>
    {
        public void Configure(EntityTypeBuilder<ProductAttributeValue> builder)
        {
            builder.HasKey(pav => new {pav.ProductId, pav.AttributeValueId, pav.Id});

            builder.HasOne(t => t.Product)
                .WithMany(pc => pc.ProductAttributeValues)
                .HasForeignKey(pc => pc.ProductId);
            builder.HasOne(t => t.AttributeValue)
                .WithMany(pc => pc.ProductAttributeValues)
                .HasForeignKey(pc => pc.AttributeValueId);
        }
    }
}