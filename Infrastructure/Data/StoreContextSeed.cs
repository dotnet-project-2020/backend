﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Infrastructure.Data.SeedData
{
    public class StoreContextSeed
    {
        public static async Task SeedAsync(StoreContext context, ILoggerFactory loggerFactory)
        {
            try
            {
                //====================================PRODUCT_BRANDS====================================
                if (!context.ProductBrands.Any())
                {
                    var data =
                        File.ReadAllText("../Infrastructure/Data/SeedData/brand.json");

                    var list = JsonConvert.DeserializeObject<List<ProductBrand>>(data);

                    foreach (var item in list)
                    {
                        context.ProductBrands.Add(item);
                    }

                    await context.SaveChangesAsync();
                }

                //====================================PRODUCT_TYPES====================================
                if (!context.ProductTypes.Any())
                {
                    var data =
                        File.ReadAllText("../Infrastructure/Data/SeedData/type.json");

                    var list = JsonConvert.DeserializeObject<List<ProductType>>(data);

                    foreach (var item in list)
                    {
                        context.ProductTypes.Add(item);
                    }

                    await context.SaveChangesAsync();
                }

                //====================================PRODUCTS====================================
                if (!context.Products.Any())
                {
                    var data =
                        File.ReadAllText("../Infrastructure/Data/SeedData/product.json");

                    var list = JsonConvert.DeserializeObject<List<Product>>(data);

                    foreach (var item in list)
                    {
                        context.Products.Add(item);
                    }

                    await context.SaveChangesAsync();
                }

                //====================================ATTRIBUTE====================================
                if (!context.Attribute.Any())
                {
                    var data =
                        File.ReadAllText("../Infrastructure/Data/SeedData/attribute.json");

                    var list = JsonConvert.DeserializeObject<List<Core.Entities.Attribute>>(data);

                    foreach (var item in list)
                    {
                        context.Attribute.Add(item);
                    }

                    await context.SaveChangesAsync();
                }

                //====================================ATTRIBUTE_VALUE====================================
                if (!context.AttributeValue.Any())
                {
                    var data =
                        File.ReadAllText("../Infrastructure/Data/SeedData/attributevalue.json");

                    var list = JsonConvert.DeserializeObject<List<AttributeValue>>(data);

                    foreach (var item in list)
                    {
                        context.AttributeValue.Add(item);
                    }

                    await context.SaveChangesAsync();
                }

                //====================================PRODUCT_DETAIL====================================
                if (!context.ProductModels.Any())
                {
                    var data =
                        File.ReadAllText("../Infrastructure/Data/SeedData/productdetail.json");

                    var list = JsonConvert.DeserializeObject<List<ProductModels>>(data);

                    foreach (var item in list)
                    {
                        context.ProductModels.Add(item);
                    }

                    await context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                var logger = loggerFactory.CreateLogger<StoreContextSeed>();
                logger.LogError(e.Message);
            }
        }
    }
}